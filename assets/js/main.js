//SLIDERS

$('.multiple-items').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    variableWidth: true,
    centerMode: true,
    autoplaySpeed: 1500
  });

$('.quotes').slick({
  infinite: true,
  autoplay: true,
  autoplaySpeed: 3000,
  arrows: true,
  prevArrow: $(".prev"),
  nextArrow: $(".next"),
});

//FULLY RESPONSIVE - BTN EFFECT
$(document).ready(function () {
  $(".button-switch").find("a").click(function (e) { 
    e.preventDefault();
    console.log("okej")
    if (!$(this).hasClass("button-arrow")) {
      $(this).toggleClass("button-arrow");
      $(this).siblings().toggleClass("button-arrow");
    };
    
  });
});
